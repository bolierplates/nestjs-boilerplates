import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  
  const config = new DocumentBuilder()
  .setTitle('Api documentation ')
  .setDescription('The auth API ')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('', app, document);
  
  
  
  app.enableCors();

  const port = process.env.PORT || 5000
  await app.listen(port);
}
bootstrap();
