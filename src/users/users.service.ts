
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/auth/dto/create-user.dto';
import { UserEntity } from './entities/user.entity';
import { UserRepository } from './user.repository';


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository : UserRepository,
  ){}

  async findOne(username: string): Promise<UserEntity | undefined> {
    return this.userRepository.findOne({username});
  }
  async findAll(){
    return this.userRepository.find()
  }
  
  async createUser(createUserDto : CreateUserDto){
    const user = new UserEntity(createUserDto)
    return  this.userRepository.save(user);
  }
}
